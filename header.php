<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>SITE | <?php echo get_the_title(); ?></title>

<!-- STYLE ZONE -->
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/swiper.min.css">  
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/normalize.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/foundation.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/hover.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/swal.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/livevalidation.css">
<!-- STYLE ZONE -->

<!-- SCRIPT ZONE -->
    <script src="<?php bloginfo('template_url'); ?>/js/swiper.jquery.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/swiper.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/foundation.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/skrollr.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/js/mandrill.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/livevalidation.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/swal.js"></script>
<!-- SCRIPT ZONE -->

	<?php wp_head(); ?>
</head>
<body>
	